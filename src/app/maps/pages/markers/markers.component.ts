import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  ViewChild,
} from '@angular/core';
import * as mapboxgl from 'mapbox-gl';

interface CustomMarker {
  color: string;
  marker: mapboxgl.Marker;
}

interface MarkerDto {
  color: string;
  lngLat: mapboxgl.LngLatLike;
}

@Component({
  selector: 'app-markers',
  templateUrl: './markers.component.html',
  styles: [
    `
      .list-group {
        position: fixed;
        top: 1.3rem;
        right: 1.3rem;
        z-index: 99;
      }

      li {
        cursor: pointer;
      }
    `,
  ],
})
export class MarkersComponent implements AfterViewInit {
  readonly MARKERS_KEY = 'markers';

  @ViewChild('myMap') divMap!: ElementRef;
  map!: mapboxgl.Map;
  zoomLevel: number = 15;
  center: mapboxgl.LngLatLike = {
    lng: -79.88477765587417,
    lat: -2.251343458529289,
  };
  markers!: CustomMarker[];

  constructor(private cd: ChangeDetectorRef) {}

  ngAfterViewInit(): void {
    this.initMap();
    this.initMarkers();
    this.cd.detectChanges();
  }

  goToMarker(marker: mapboxgl.Marker) {
    this.map.flyTo({ center: marker.getLngLat() });
  }

  addMarker() {
    const color = '#xxxxxx'.replace(/x/g, (y) =>
      ((Math.random() * 16) | 0).toString(16)
    );
    const marker = this.createMarker(this.center, color);
    this.markers.push({ color, marker });
    this.setMarkersOnLocal();
  }

  removeMarker(i: number) {
    console.log('has');
    this.markers[i].marker.remove();
    this.markers.splice(i, 1);
    this.setMarkersOnLocal();
  }

  private initMap() {
    this.map = new mapboxgl.Map({
      container: this.divMap.nativeElement,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: this.center,
      zoom: this.zoomLevel,
    });
  }

  private initMarkers() {
    this.markers = this.mapMarkersDto(this.getMarkersFromLocal());
  }

  private setMarkersOnLocal(): void {
    const markers = this.markers.map(({ marker, color }): MarkerDto => {
      return { lngLat: marker.getLngLat(), color };
    });
    const markersJson = JSON.stringify(markers);
    localStorage.setItem(this.MARKERS_KEY, markersJson);
    console.log('MARKERS SETTED ON LOCAL STORAGE');
  }

  private getMarkersFromLocal(): MarkerDto[] {
    const markersJson = localStorage.getItem(this.MARKERS_KEY);
    if (!markersJson) {
      return [];
    }
    return JSON.parse(markersJson) as MarkerDto[];
  }

  private mapMarkersDto(markersDto: MarkerDto[]): CustomMarker[] {
    return markersDto.map(({ lngLat, color }): CustomMarker => {
      const marker = this.createMarker(lngLat, color);
      return { color, marker };
    });
  }

  private createMarker(
    lngLat: mapboxgl.LngLatLike,
    color: string,
    draggable: boolean = true
  ) {
    return new mapboxgl.Marker({
      draggable,
      color,
    })
      .setLngLat(lngLat)
      .addTo(this.map)
      .on('dragend', () => {
        this.setMarkersOnLocal();
      });
  }
}
