import {
  AfterViewInit,
  Component,
  ElementRef,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import * as mapboxgl from 'mapbox-gl';

@Component({
  selector: 'app-zoom-range',
  templateUrl: './zoom-range.component.html',
  styles: [
    `
      .row {
        background-color: white;
        border-radius: 0.3rem;
        bottom: 3rem;
        left: 3rem;
        padding: 0.7rem;
        position: fixed;
        z-index: 999;
      }
    `,
  ],
})
export class ZoomRangeComponent implements AfterViewInit, OnDestroy {
  readonly MIN_ZOOM = 0;
  readonly MAX_ZOOM = 18;

  @ViewChild('myMap') divMap!: ElementRef;
  map!: mapboxgl.Map;
  zoomLevel: number = 10;
  center: [number, number] = [-79.88477765587417, -2.251343458529289];

  ngAfterViewInit(): void {
    this.initMap();
    this.initListeners();
  }

  ngOnDestroy(): void {
    this.clearListeners();
  }

  zoomOut() {
    this.map.zoomOut();
  }

  zoomIn() {
    this.map.zoomIn();
  }

  zoomChange(value: string) {
    this.map.zoomTo(Number(value));
  }

  private initMap() {
    this.map = new mapboxgl.Map({
      container: this.divMap.nativeElement,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: this.center,
      zoom: this.zoomLevel,
    });
  }

  private initListeners() {
    this.map.on('zoom', () => {
      this.zoomLevel = this.map.getZoom();
    });
    this.map.on('zoomend', () => {
      if (this.map.getZoom() > 18) {
        this.map.zoomTo(18);
      }
    });
    this.map.on('move', (event) => {
      const { target } = event;
      const { lng, lat } = target.getCenter();
      this.center = [lng, lat];
    });
  }

  private clearListeners() {
    this.map.off('zoom', () => {});
    this.map.off('zoomend', () => {});
    this.map.off('move', () => {});
  }
}
