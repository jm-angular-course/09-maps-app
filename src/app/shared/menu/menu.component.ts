import { Component } from '@angular/core';

class MenuItem {
  constructor(public route: string, public name: string) {}
}

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styles: [
    `
      li {
        cursor: pointer;
      }
    `,
  ],
})
export class MenuComponent {
  menuItems: MenuItem[] = [
    new MenuItem('/maps/fullscreen', 'Fullscreen'),
    new MenuItem('/maps/zoom-range', 'Zoom Range'),
    new MenuItem('/maps/markers', 'Marcadores'),
    new MenuItem('/maps/properties', 'Propiedades'),
  ];
}
